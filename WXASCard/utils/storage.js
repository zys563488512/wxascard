
// 异步存储
function saveToStorageAsync(key,data){
  try{
    wx.setStorage({
      key: key,
      data: data
    })
    return true
  }catch(e){
    console.error(e)
    return false
  }
    
}
// 异步读取
function getInfoStorageAsync(key){
  try{
    

    wx.getStorage({
      key: key,
      success: function(res) {
        return res
      },
    })
  }catch(e){
    console.error(e)
    return null
  }
}
// 删除异步存储
function removeStorageAsync(key){
  try{
    wx.removeStorage({
      key: key,
      success: function(res) {
        return res
      },
    })
  }catch(e){
    console.error(e)
    return null
  }
}

// 同步存储
function saveStorageSync(key,data) {
  try{
    wx.setStorageSync(key, data)
    return true
  }catch(e){
    console.error(e)
    return false
  }
}

// 同步读取
function getInfoStorageSync(key){
  try {
    var res = wx.getStorageSync(key)
    if(res){
      return res
    }else{
      return null
    }
  }catch(e){
    console.error(e)
    return null
  }
}

// 删除同步存储
function removeStorageSync(key){
  try {
    var res = wx.removeStorageSync(key)
    console.log(res.keys)
    console.log(res.currentSize)
    console.log(res.limitSize)
  }catch (e){
    console.error(e)
    return null
  }
}


// 清理本地数据异步执行
function clearLocalStorageAsync(){
  try{
    wx.clearStorage()
    return true
  }catch(e){
    console.error(e)
    return false
  }
}
// 清理本地数据同步执行
function clearLocalStorageSync(){
  try{
    wx.clearStorageSync()
    return true
  }catch(e){
    console.error(e)
    return false
  }
}

module.exports = {
  saveToStorage: saveToStorageAsync,
  getInfoInStorageAsync: getInfoStorageAsync,
  saveStorageSync: saveStorageSync,
  getInfoStorageSync: getInfoStorageSync,
  removeStorageAsync: removeStorageAsync,
  removeStorageSync: removeStorageSync,
  clearLocalStorageAsync: clearLocalStorageAsync,
  clearLocalStorage: clearLocalStorageSync 
}