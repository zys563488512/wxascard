// var rootDocment = 'https://weshop.ifdiy.com/' //域名 
var rootDocment = 'https://cards.yifubank.com/' //域名(测试)
var wxStorage = require('../utils/storage.js')

function req(url, data, method, response) {
  // 判断网络
  wx.getNetworkType({
    success: function(res) {
      var networkType = res.networkType
      if (networkType == "none") {
        wx.showModal({
          title: '提示',
          content: '没有连接网络',

          showCancel: false
        })
      } else {
        wx.showLoading();
        console.log('data == ' + data)
        console.log('url ===' + rootDocment + url)
        wx.request({
          url: rootDocment + url,
          data: data,
          // header: {
          //   'content-type': 'application/json' // 默认值
          // },
          method: method, //POST
          success: function(res) {
            wx.hideLoading();

            if (res.statusCode == 404) {
              wx.navigateBack({

              })
              wx.navigateTo({
                url: '../error/error',
              })

            } else if (res.statusCode == 500) {
              wx.showModal({
                title: '服务器错误',
                content: res.data.msg + '',
                showCancel: false
              })

            } else if (res.statusCode == 401) {
              wx.showModal({
                title: '服务器错误',
                content: res.data.msg + '',
              })
            } else if (res.statusCode != 200 && res.statusCode != 404 && res.statusCode != 500) {
              wx.navigateBack({

              })
              wx.showModal({

                title: '服务器错误',
                content: res.data.msg,
                showCancel: false
              })
            }
            return typeof response == "function" && response(res)
          },
          fail: function() {
            return typeof response == "function" && response(false)
          }
        })

      }
    },
  })

}

module.exports = {
  req: req
}



