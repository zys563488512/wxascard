// pages/index/bankList/bankList.js
var wxStorage = require('../../../utils/storage.js')
 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bankArray:[],
    chooseCard:{},
    isDelete:false,
    isFrom:'',
    bankImage: {
      xyyh: "兴业银行",
      pfyh: "浦发银行",
      nyyh: "农业银行",
      gdyh: "光大银行",
      zgyh: "中国银行",
      gsyh: "工商银行",
      jsyh: "建设银行",
      bjyh: "北京银行",
      jlyh: "吉林银行",
      gzyh: "广州银行",
      gznysyyh: "广州农村商业银行",
      zhhryh: "珠海华润银行",
      qlyh: "齐鲁银行",
      jssyh: "江苏银行",
      njyh: "南京银行",
      hrbyh: "哈尔滨银行",
      wsyh: "徽商银行",
      lzyh: "兰州银行",
      nxyh: "宁夏银行",
      bhyh: "渤海银行",
      hbyh: "河北银行",
      xayh: "西安银行",
      jtyh: "交通银行",
      // dlyh: "大连银行",
      zzyh: "郑州银行",
      nbyh: "宁波银行",
      zxyh: "中信银行",
      hxyh: "华夏银行",
      shyh: "上海银行",
      gfyh: "广发银行",
      payh: "平安银行",
      msyh: "民生银行",
      zsyh: "招商银行",
      bjns: "北京农商",
      gzns: "广州农商",
      jjyh: "九江银行"
    }
  },
  chooseBankCard: function (e) {
    var that = this;
    if(that.data.isFrom == "mine") {

    }else {
      let pages = getCurrentPages(); //获取当前页面js里面的pages里的所有信息。
      let prevPage = pages[pages.length - 2];
      console.log(e.currentTarget.dataset.bean)
      prevPage.setData({
        bank: e.currentTarget.dataset.bean,
        isChange: true
      })
      wx.navigateBack({
        delta: 1
      })
    }
    
  },
  getBankList: function () {
    var that = this;
    console.log("mch/viewCardPackage")
    wx.request({
      url: 'https://cards.yifubank.com/mch/viewCardPackage',
      data: {
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log('response.statusCode' + response.statusCode)
        console.log(response)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            var array = new Array()
            for (let i in response.data.list) {
              if (response.data.list[i].account_bank) {
                var dict = {};
                for (let j in that.data.bankImage) {
                  if (response.data.list[i].account_bank == that.data.bankImage[j]) {
                    
                    dict = {
                      bankUrl: 'https://cards.yifubank.com/xiaochengxu_image/small_bank_icon/' + j + '.png',
                        account_bank:response.data.list[i].account_bank,
                        account_no: response.data.list[i].account_no,
                        card_id: response.data.list[i].card_id,
                        cert_no: response.data.list[i].cert_no,
                        mobile: response.data.list[i].mobile,
                        name: response.data.list[i].name
                      }
                    console.log(dict.bankUrl)
                  }
                }
                array[i] = dict
              }
            }
            that.setData({
              bankArray: array
            })
          }else{
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    let pages = getCurrentPages(); //获取当前页面js里面的pages里的所有信息。
    let prevPage = pages[pages.length - 2];
    prevPage.setData({
      isChange: false
    })
    that.setData({
      isFrom: options.isFrom
    })
  },
  deleteBank: function(e) {
    var that = this;
    wx.showModal({
      title: '解绑提示',
      content: '您确定要解绑该卡吗？',
      success(res) {
        console.log(e.currentTarget.id)
        var sss = {
          "card_id": e.currentTarget.id
        }
        console.log("mch/getTunnelMsg")
        wx.request({
          url: 'https://cards.yifubank.com/mch/unbindCard',
          data: {
            jsonPara: JSON.stringify(sss),
            token: wxStorage.getInfoStorageSync('token')
          },
          method: 'POST',
          success(response) {
            wx.hideLoading()
            if (response.statusCode == 200) {
              if (response.data.code == "0000") {
                wx.showToast({
                  title: '解绑成功',
                })
                that.setData({
                  isDelete: true
                })
                that.getBankList()
              } else {
                wx.showModal({
                  title: '',
                  content: response.data.msg + '',
                  showCancel: false
                })
              }
            } else {
              wx.showModal({
                title: '服务器错误',
                content: response.data.msg + '',
                showCancel: false
              })
            }
          }
        })
      }
    })
    // wx.hideLoading()
  },
  addbankCard: function () {
    wx.navigateTo({
      url: '../addBankCard/addBankCard?isFrom=' + 'bankList',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.showLoading({
      title: '',
    })
    var that = this;
    that.getBankList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})