// pages/index/addBankCard/addBankCard.js
var wxStorage = require('../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bankName:'',
    bankNumber:'',
    account_bank:'工商银行',
    mobile:'',
    cvnNumber:'',
    bankArray: ["兴业银行",
      "浦发银行",
      "农业银行",
      "光大银行",
      "中国银行",
      "工商银行",
      "建设银行",
      "北京银行",
      "吉林银行",
      "广州银行",
      "广州农村商业银行",
      "珠海华润银行",
      "齐鲁银行",
      "江苏银行",
      "南京银行",
      "哈尔滨银行",
      "徽商银行",
      "兰州银行",
      "宁夏银行",
      "渤海银行",
      "河北银行",
      "西安银行",
      // "大连银行",
      "郑州银行",
      "宁波银行",
      "中信银行",
      "华夏银行",
      "上海银行",
      "招商银行",
      "广发银行",
      "平安银行",
      "民生银行",
      "北京农商",
      "广州农商"],
    isFrom:'',
    oldTime:''
  },
  bindPickerChange: function (e) {
    var that = this;
    console.log("bankName ---" + that.data.bankArray[e.detail.value])
    that.setData({
      account_bank: that.data.bankArray[e.detail.value]
    })
  },
  /**
   * 事件处理
   */

  bindSave: function (e) {
    var that = this;
    that.data.bankNumber = e.detail.value.bankNumber
    that.data.mobile = e.detail.value.mobile
    that.data.cvnNumber = e.detail.value.cvnNumber
    that.data.oldTime = e.detail.value.oldTime
    var sss = {
      "card_id": '',
      "account_no": that.data.bankNumber,
      "account_bank": that.data.account_bank,
      "mobile": that.data.mobile,
      "expired": that.data.oldTime,
      "cvn2": that.data.cvnNumber
    }
    var data = {
      jsonPara: JSON.stringify(sss),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    wx.request({
      url: 'https://cards.yifubank.com/mch/bandTradeCard',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        console.log('response.code' + response.code)
        console.log('response.statusCode' + response.statusCode)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            if (that.data.isFrom == "") {
              wx.reLaunch({
                url: '../deal/deal',
              })
            }else {
              wx.navigateBack({
                
              })
            }
          }else {
            wx.showModal({
              title: '',
              content: response.data.msg,
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this;
    that.setData({
      isFrom: options.isFrom
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})