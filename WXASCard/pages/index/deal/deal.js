// pages/index/deal/deal.js
var wxStorage = require('../../../utils/storage.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isFQ: true,
    isFQ12:true,
    isFQ24:true,
    isShow12:'',
    isShow24:'',
    array: [],
    qd:"",
    single_limit:'',
    day_limit:'',
    qdArray:[],
    money:'',
    orderNo:'',
    bank:{},
    bankUrl:'',
    yiBao:false,
    isMoney:false,
    isChange:false,
    yan:'',
    scrollLeft:1000,
    ifName: false,
    ifRed: false,
    tunnelDict:{},
    heightCell:300,
    bankImage: {
      xyyh: "兴业银行",
      pfyh: "浦发银行",
      nyyh: "农业银行",
      gdyh: "光大银行",
      zgyh: "中国银行",
      gsyh: "工商银行",
      jsyh: "建设银行",
      bjyh: "北京银行",
      jlyh: "吉林银行",
      gzyh: "广州银行",
      gznysyyh: "广州农村商业银行",
      zhhryh: "珠海华润银行",
      qlyh: "齐鲁银行",
      jssyh: "江苏银行",
      njyh: "南京银行",
      hrbyh: "哈尔滨银行",
      wsyh: "徽商银行",
      lzyh: "兰州银行",
      nxyh: "宁夏银行",
      bhyh: "渤海银行",
      hbyh: "河北银行",
      xayh: "西安银行",
      jtyh: "交通银行",
      // dlyh: "大连银行",
      zzyh: "郑州银行",
      nbyh: "宁波银行",
      zxyh: "中信银行",
      hxyh: "华夏银行",
      shyh: "上海银行",
      gfyh: "广发银行",
      payh: "平安银行",
      msyh: "民生银行",
      zsyh: "招商银行",
      bjns: "北京农商",
      gzns: "广州农商",
      jjyh: "九江银行"
    },
    bankArray:[]
  },
  // 卡列表
  getToBankList: function() {
    console.log('getToBankList')
    var that = this;
    that.setData({
      bank:{},
      single_limit:""
    })
    wx.navigateTo({
      url: '../bankList/bankList',
    })
  },
  setValue: function(e) {
    var that = this;
    this.setData({
      yan: e.detail.value
    })
    console.log('yan----' + that.data.yan)
  },
  cancel: function () {
    var that = this;
    that.setData({
      ifName:false,
      isMoney:false
    })
  },
  confirm: function() {
    var that = this;
    if (that.data.yan == '') {
      wx.showToast({
        title: '请输入验证码',
      })
      return
    }
    that.cancel()
    wx.showLoading({
      title: '正在请求交易',
    })
    var tunnelId = that.data.tunnelDict.tunnel_id;
    var installNum = ""
    if (tunnelId == "7") {
      installNum = "12"
    } else if (tunnelId == "8") {
      installNum = "24"
    }
    if(tunnelId == '3') {
      var sss = {
        "card_id": that.data.bank.card_id,
        "verifyCode": that.data.yan
      }
      console.log("----------渠道id-------------")
      console.log(sss)
      wx.request({
        url: 'https://cards.yifubank.com/mch/confirmTreatyCollectApply',
        data: {
          jsonPara: JSON.stringify(sss),
          token: wxStorage.getInfoStorageSync('token')
        },
        method: 'POST',
        success(response) {
          wx.hideLoading()
          console.log('response.statusCode' + response.statusCode)
          console.log(response)
          if (response.statusCode == 200) {
            if (response.data.code == "0000") {
              wx.showModal({
                title: '绑定提醒',
                content: '绑定成功请重新发起交易',
                showCancel: false
              })
              
            } else {
              wx.showModal({
                title: '交易失败',
                content: response.data.msg + '',
                showCancel: false
              })
            }
          } else {
            wx.showModal({
              title: '服务器错误',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        }
      })
    }else {
      var sss = {
        "card_id": that.data.bank.card_id,
        "trade_no": that.data.orderNo,
        "installNum": installNum,
        "tunnel_id": tunnelId,
        "verifyCode": that.data.yan
      }
      console.log("----------渠道id-------------")
      console.log(sss)
      wx.request({
        url: 'https://cards.yifubank.com/mch/submitKfqMsg',
        data: {
          jsonPara: JSON.stringify(sss),
          token: wxStorage.getInfoStorageSync('token')
        },
        method: 'POST',
        success(response) {
          wx.hideLoading()
          console.log('response.statusCode' + response.statusCode)
          console.log(response)
          if (response.statusCode == 200) {
            if (response.data.code == "0000") {
              wx.showModal({
                title: '交易成功',
                content: response.data.msg + '',
                showCancel: false
              })
              that.setData({
                yan: ''
              })
              wx.switchTab({
                url: '../orderList/orderList',
              })
            } else {
              wx.showModal({
                title: '交易失败',
                content: response.data.msg + '',
                showCancel: false
              })
            }
          } else {
            wx.showModal({
              title: '服务器错误',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        }
      })
    }
    
  },
  // 提现
  bindSave: function (e) {
    var that = this;
    
    console.log('发起交易')
    that.data.money = e.detail.value.money
    if (that.data.money == '') {
      wx.hideLoading()
      wx.showToast({
        title: '请输入提现金额',
      })
      return
    }
    wx.showModal({
      title: '确认信息',
      content: '提现金额: ' + that.data.money + "元\n" + '选择渠道: ' + that.data.qd,
      showCancel: true,//是否显示取消按钮
      confirmText: "确定",//默认是“确定”
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          //点击确定
          wx.showLoading({
            title: '',
          })
          if (that.data.money < that.data.tunnelDict.min) {
            wx.hideLoading()
            wx.showModal({
              title: '提现提示',
              content: "提现金额不能小于" + that.data.tunnelDict.min,
            })
            return
          }
          if (that.data.money > that.data.tunnelDict.max) {
            wx.hideLoading()
            wx.showModal({
              title: '提现提示',
              content: "提现金额不能大于" + that.data.tunnelDict.max,
            })
            return
          }
          var sss = {
            "total_fee": that.data.money,
            "card_id": that.data.bank.card_id
          }
          var tunnelId = that.data.tunnelDict.tunnel_id;
          console.log(tunnelId)
          if (tunnelId == "1") {
            wx.hideLoading()
            // tunneName = "商超类"
          } else if (tunnelId == "2") {
            // tunneName = "航旅类" 易宝
            wx.hideLoading()

            wx.request({
              url: 'https://cards.yifubank.com/mch/createYbOrder',
              data: {
                jsonPara: JSON.stringify(sss),
                token: wxStorage.getInfoStorageSync('token')
              },
              method: 'POST',
              success(response) {
                wx.hideLoading()
                console.log('response.statusCode' + response.statusCode)
                console.log(response)
                if (response.statusCode == 200) {
                  console.log('data------' + response.data)
                  if (response.data.code == "0000") {
                    console.log('data------' + response)
                    wx.setClipboardData({
                      data: response.data.url,
                    })
                    wx.showModal({
                      title: '交易提醒',
                      content: '航旅类暂不支持小程序直接操作，请前往手机浏览器粘贴链接进行下一步操作',
                    })
                  } else {
                    wx.showModal({
                      title: '交易提醒',
                      content: response.data.msg + '',
                      showCancel: false
                    })
                  }
                } else {
                  wx.showModal({
                    title: '服务器错误',
                    content: response.data.msg + '',
                    showCancel: false
                  })
                }
              }
            })
          } else if (tunnelId == "3") {
            // tunneName = "数娱类"
            wx.hideLoading()
            wx.request({
              url: 'https://cards.yifubank.com/mch/createKftOrder',
              data: {
                jsonPara: JSON.stringify(sss),
                token: wxStorage.getInfoStorageSync('token')
              },
              method: 'POST',
              success(response) {
                wx.hideLoading()
                console.log('response.statusCode' + response.statusCode)
                console.log(response)
                if (response.statusCode == 200) {
                  console.log('data------' + response.data)
                  if (response.data.code == "0000") {
                    console.log('data------' + response)
                    if(response.data.isBind == 0) {
                      wx.showModal({
                        title: '绑定提醒',
                        content: '第一次交易需先绑定，绑定后再次发起交易',
                        showCancel: false
                      })
                      that.getYan();
                    }else {
                      wx.navigateTo({
                        url: '../orderStatus/orderStatus?status=success', 
                      })
                    }
                  } else {
                    wx.showModal({
                      title: '交易提醒',
                      content: response.data.msg + '',
                      showCancel: false
                    })
                  }
                } else {
                  wx.showModal({
                    title: '服务器错误',
                    content: response.data.msg + '',
                    showCancel: false
                  })
                }
              }
            })
          } else if (tunnelId == "5") {
            // tunneName = "小微类"
          } else {
            // (tunnelId == "6" || tunnelId == "7" || tunnelId == "8"
            console.log("---------" + tunnelId + "--------------")
            var installNum = ""
            if (tunnelId == "7") {
              installNum = "12"
            } else if (tunnelId == "8") {
              installNum = "24"
            }
            sss = {
              "total_fee": that.data.money,
              "installNum": installNum,
              "card_id": that.data.bank.card_id,
              "isUse": "1"
            }
            console.log('美容类')
            console.log('---------' + sss + '------------')
            wx.request({
              url: 'https://cards.yifubank.com/mch/createKfqOrder',
              data: {
                jsonPara: JSON.stringify(sss),
                token: wxStorage.getInfoStorageSync('token')
              },
              method: 'POST',
              success(response) {
                wx.hideLoading()
                console.log('response.statusCode' + response.statusCode)
                console.log(response)
                if (response.statusCode == 200) {
                  if (response.data.code == "0000") {
                    that.setData({
                      ifName: true,
                      isMoney: true,
                      orderNo: response.data.trade_no
                    })
                    // that.getBankList()
                  } else {
                    wx.showModal({
                      title: '交易失败',
                      content: response.data.msg + '',
                      showCancel: false
                    })
                  }
                } else {
                  wx.showModal({
                    title: '服务器错误',
                    content: response.data.msg + '',
                    showCancel: false
                  })
                }
              }
            })
          }
          that.setData({
            money: ''
          })
        }
      },
      fail: function (res) { },//接口调用失败的回调函数
      complete: function (res) { },//接口调用结束的回调函数（调用成功、失败都会执行）
    })  
  },
  getYan: function() {
    wx.showLoading({
      title: '',
    })
    var that = this;
    var sss = {
      card_id: that.data.bank.card_id
    }
    wx.request({
      url: 'https://cards.yifubank.com/mch/treatyCollectApply',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log('response.statusCode' + response.statusCode)
        console.log(response)
        if (response.statusCode == 200) {
          console.log('data------' + response.data)
          if (response.data.code == "0000") {
            wx.showModal({
              title: '提醒',
              content: '验证码已发',
              showCancel: false
            })
            that.setData({
              ifName:true
            })
          } else {
            wx.showModal({
              title: '交易提醒',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('onLoad')
    var that = this;
    // var sss = {
    //   version: '1.6'
    // }
    // console.log('mch/getXiaoChenXu')
    // wx.request({
    //   url: 'https://cards.yifubank.com/getXiaoChenXu',
    //   data: sss,
    //   method: 'POST',
    //   success(response) {
    //     wx.hideLoading()
    //     console.log(response)
    //     if (response.statusCode == 200) {
    //       if (response.data == "0") {
    //         wx.navigateTo({
    //           url: '../shell/shell',
    //         })
    //         return
    //       }else {
            
    //       }
    //     } else {
    //       wx.showModal({
    //         title: '服务器错误',
    //         content: response.data.msg + '',
    //         showCancel: false
    //       })
    //       return
    //     }
    //   }
    // })
    var activity = wxStorage.getInfoStorageSync('activity')
    if (activity != null) {
      that.setData({
        ifRed: true,
        isMoney: true
      })
    }
  },
  /**
   * 点击红包图片
   */ 
  getRedPacket: function () {
    var that = this;
    wxStorage.saveStorageSync('activity',null)
    that.setData({
      ifRed: false,
      isMoney: true
    })
  },
  /**
 * 生命周期函数--监听页面初次渲染完成
 */
  onReady: function () {
    console.log('onReady')
  },

  getQDList: function () {
    var that = this;
    var dic = that.data.bank
    
    // for (let i in this.cardList) {
    //   if (this.cardList[i].account_bank) {
        for (let j in that.data.bankImage) {
          if (that.data.bank.account_bank == that.data.bankImage[j]) {
            that.setData({
              bankUrl:'https://cards.yifubank.com/xiaochengxu_image/big_bank_icon/' + j + '.png'
            })
          }
        }
    //   }
    // }

    var sss = {
      "account_bank": dic.account_bank,
      "card_id": dic.card_id
    }
    console.log("mch/getTunnelMsg")
    wx.request({
      url: 'https://cards.yifubank.com/mch/getTunnelMsg',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        console.log('----getTunnelMsg----')
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              qd: '',
              qdArray: response.data.list
            })
            console.log("mch/getTradeType")
            wx.request({
              url: 'https://cards.yifubank.com/mch/getTradeType?1',
              data: {
                jsonPara: JSON.stringify(sss),
                token: wxStorage.getInfoStorageSync('token')
              },
              method: 'POST',
              success(response) {
                wx.hideLoading()
                console.log("------------------")
                console.log(response)
                console.log('================')
                if (response.statusCode == 200) {
                  if (response.data.code == "0000") {
                    var arr = Array()
                    var index = 0
                    var dict = {}
                    for(var i = 0;i < that.data.qdArray.length; i++) {
                      for(var j= 0; j < response.data.data.length;j++) {
                        if(that.data.qdArray[i].tunnel_id == response.data.data[j].tunnel_id){
                          // console.log('1111111111111111')
                          // console.log(response.data.data[j])
                          if (response.data.data[j].number == "12") {
                            // response.data.data[j].name = '分期还款(12期,手续费为6.5%)'
                            response.data.data[j].font_style = '手续费8折，仅6.5%'
                            that.setData({
                              isShow12:'www1111',
                              isFQ: false,
                              isFQ12: false,
                            })
                          } else if (response.data.data[j].number == '24') {
                            // response.data.data[j].name = '分期还款(24期,手续费为12.5%)'
                            response.data.data[j].font_style = '手续费8折，仅12.5%'
                            that.setData({
                              isShow12:'www1112',
                              isShow24: 'www2222',
                              isFQ: false,
                              isFQ24: false,
                            })
                          }
                          if(that.data.qdArray[i].tunnel_id == '6'){
                            if (arr.length > 0) {
                              var ss = arr[0];
                              arr[0] = response.data.data[j];
                              arr[index] = ss;
                              console.log(arr)
                            }else {
                              arr[index] = response.data.data[j];
                            }
                          }else {
                            arr[index] = response.data.data[j];
                            console.log(arr)
                          }
                          index += 1;
                        }
                      }
                      // if (arr.length > 0) {
                      //   console.log(arr)
                      //   for (var i = 0; i < arr.length; i++) {
                      //     if (arr[i].style.length > 0) {
                      //       arr[i].name = arr[i].name + arr[i].style
                      //     }
                      //   }
                      // }
                      

                      if(arr[0].number == '12') {
                        var ss=''
                        if (that.data.isShow12 == 'www1111'){
                          ss='www1111-select'
                        }else {
                          ss='www1112-select'
                        }
                        that.setData({
                          qd: '分期还款(12期,手续费为6.5%)',
                          isShow12:ss
                        })
                      }else if(arr[0].number == '24'){
                        that.setData({
                          qd: '分期还款(24期,手续费为12.5%)',
                        })
                      }else {
                        that.setData({
                          qd: arr[0].name,
                        })
                      }
                      that.setData({
                        array: arr,
                        heightCell: arr.length * 30,
                        single_limit:arr[0].desc,
                        day_limit:arr[0].max,
                        tunnelDict: arr[0]
                      })
                      console.log(that.data.array)
                      console.log(that.data.tunnelDict)
                    }
                  } else {
                    wx.showModal({
                      title: '',
                      content: response.data.msg + '',
                      showCancel: false
                    })
                  }
                } else {
                  wx.showModal({
                    title: '服务器错误',
                    content: response.data.msg + '',
                    showCancel: false
                  })
                }
              }
            })
          }else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  getLoginAuth:function() {
    console.log('getLoginAuth')
    var that = this;
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          console.log('已经授权')
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          that.getLoginAouth()
        } else {
          console.log('未授权')
          wx.authorize({
            scope: '',
            success(res) {
              that.getLoginAouth()
            }
          })
        }
      }
    })
  },
  getLoginAouth: function () {
    console.log('getLoginAouth')
    var that = this;
    // if (wxStorage.getInfoStorageSync('token') == null && wxStorage.getInfoStorageSync('cert') == "0000") {
      var loginCode = ''
      // 登录
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          loginCode = res.code
          wx.showLoading({
            title: '',
          })
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              // this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
              console.log("mch/xiaochengxu")

              console.log('-----------------------')
              var code = wxStorage.getInfoStorageSync('wxCode')
              console.log(code)
              var data = {}
              if(code == null) {
                data = {
                  js_code: loginCode,
                  obj: res
                }
              }else {
                data = {
                  js_code: loginCode,
                  agent_id: code,
                  obj: res
                }
              }
              
              console.log(data)
              wx.request({
                url: 'https://cards.yifubank.com/xiaochengxu',
                data: data,          
                method: 'POST',
                success(response) {
                  wx.hideLoading()
                  console.log(response)
                  if (response.statusCode == 200) {
                    console.log('token')
                    wxStorage.saveStorageSync('token', response.data.token)
                    console.log(response.data.token)
                    if (response.data.code == "0000") {
                      
                      // wxStorage.saveStorageSync('cert', "0000")
                      if(response.data.step == "1") {
                        wx.reLaunch({
                          url: '../register/register',
                        })
                      } else if (response.data.step == "0"){
                        that.getBankList()
                      } else if (response.data.step == "2") {
                        wx.reLaunch({
                          url: '../aouth/aouth',
                        })
                      } else if (response.data.step == "3") {
                        wx.reLaunch({
                          url: '../certification/certification',
                        })
                      }
                    } else {
                      // wxStorage.saveStorageSync('cert', "0003")
                      wx.reLaunch({
                        url: '../register/register',
                      })
                    }
                  } else {
                    wx.showModal({
                      title: '服务器错误',
                      content: response.data.msg + '',
                      showCancel: false
                    })
                  }
                }
              })
            }
          })
          // wx.hideLoading()
        }
      })
    // } else {
    //   console.log("已登录")
    // }
  },
  getBankList : function() {
    var that = this;
    console.log("mch/viewCardPackage")
    wx.request({
      
      url: 'https://cards.yifubank.com/mch/viewCardPackage',
      data: {
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log('response.statusCode' + response.statusCode)
        console.log(response)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            if(response.data.list.length > 0) {
              that.setData({
                bankArray: response.data.list,
                bank: response.data.list[0]
              })
              that.getQDList()
            }else {
              wx.navigateTo({
                url: '../addBankCard/addBankCard?isFrom=' + 'bankList',
              })
            }
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  onShow: function () {
    var that = this;
    if (that.data.isChange) {
      that.getQDList()
    } else {
      wx.showLoading({
        title: '',
      })
      that.getLoginAuth()
    }
    // var sss = {
    //   version: '1.6'
    // }
    // console.log('mch/getXiaoChenXu')
    // wx.request({
    //   url: 'https://cards.yifubank.com/getXiaoChenXu',
    //   data: sss,
    //   method: 'POST',
    //   success(response) {
    //     wx.hideLoading()
    //     console.log(response)
    //     if (response.statusCode == 200) {
    //       if (response.data == "0") {
    //         wx.navigateTo({
    //           url: '../shell/shell',
    //         })
    //         return
    //       } else {

    //         if (that.data.isChange) {
    //           that.getQDList()
    //         } else {
    //           wx.showLoading({
    //             title: '',
    //           })
    //           that.getLoginAuth()
    //         }
    //       }
    //     } else {
    //       wx.showModal({
    //         title: '服务器错误',
    //         content: response.data.msg + '',
    //         showCancel: false
    //       })
    //       return
    //     }
    //   }
    // })
  },
  showModal: function () {
    // 显示遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showModalStatus: true
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
  },
  hideModal: function () {
    // 隐藏遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "linear",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: false
      })
    }.bind(this), 200)
  },
  chooseQD:function(e){
    var that = this;
    that.hideModal();
    var qdStr = ""
    console.log(e.currentTarget.dataset.name)
    if (e.currentTarget.dataset.name.style.length > 0) {
      qdStr = e.currentTarget.dataset.name.name + "(" + e.currentTarget.dataset.name.style + ")"
    }else {
      qdStr = e.currentTarget.dataset.name.name
    }
    var ss = ''
    if(e.currentTarget.dataset.name.number == '12') {
      that.getChooseQDDD();
    } else if (e.currentTarget.dataset.name.number == '24') {
      that.getChooseQDDDD();
    }else {
      if (that.data.isFQ24 == false) {
        that.setData({
          isShow12:'www1112',
          isShow24:'www2222'
        })
      }else {
        that.setData({
          isShow12:'www1111'
        })
      }
    }
    that.setData({
      qd: qdStr,
      single_limit: e.currentTarget.dataset.name.desc,
      tunnelDict: e.currentTarget.dataset.name
    });
    
    console.log(e);
  },
  getChooseQDDDD:function(){
    var that = this;
    for (var i = 0; i < that.data.array.length;i++){
      if (that.data.array[i].number == '24'){
        that.setData({
          tunnelDict: that.data.array[i]
        })
      }
    }
    that.setData({
      isShow12: 'www1112',
      isShow24: 'www2222-select',
      qd:'分期还款(24期，手续费为12.5%)'
      
    })
  },
  getChooseQDDD:function() {
    var that = this;
    for (var i = 0; i < that.data.array.length; i++) {
      if (that.data.array[i].number == '12') {
        that.setData({
          tunnelDict: that.data.array[i]
        })
      }
    }
    if (that.data.isFQ24 == false) {
      that.setData({
        isShow12: 'www1112-select',
        isShow24: 'www2222',
        qd: '分期还款(12期，手续费为6.5%)'
      })
    }else {
      that.setData({
        isShow12: 'www1111-select',
        isShow24: 'www2222',
        qd: '分期还款(12期，手续费为6.5%)'
      })
    }
  }
})