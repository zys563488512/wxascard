// pages/order/orderList/orderList.js
var wxStorage = require('../../../utils/storage.js')
var pageIndex = 1
var pageNumber = 20
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currDate: '本月',
    allMoney: '',
    dailyMoney: '',
    dataArray: [],
    scrollTop: 0,
    scrollHeight: 0,
    searchLoading:false,
    searchLoadingComplete: false,
    scrollHeight: "100%"
  },
  /**
   * 上下拉
   */
  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    wx.showLoading({
      title: '玩命加载中',
    })
    var that = this;
    pageIndex = 1;
    var sss = {
      "page": pageIndex,
      "num": pageNumber,
      "condition": {
        "date": that.data.currDate,
        "dateType": 'month'
      }
    }
    that.setData({
      dataArray: []
    })
    console.log('------------')
    console.log(that.data.dataArray)
    var data = {
      jsonPara: JSON.stringify(sss),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('viewTradeList')
    wx.request({
      url: 'https://cards.yifubank.com/mch/viewTradeList',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              dataArray: response.data.list
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
    wx.stopPullDownRefresh()
    wx.hideNavigationBarLoading()
  },
  onReachBottom: function () {
    // // 显示加载图标
    wx.showLoading({
      title: '玩命加载中',
    })
    var that = this;
    pageIndex += 1;
    that.setData({
      searchLoading:true
    })
    var sss = {
      "page": pageIndex,
      "num": pageNumber,
      "condition": {
        "date": that.data.currDate,
        "dateType": 'month'
      }
    }

    var data = {
      jsonPara: JSON.stringify(sss),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('viewTradeList')
    console.log(wxStorage.getInfoStorageSync('token'))
    wx.request({
      url: 'https://cards.yifubank.com/mch/viewTradeList',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            if(response.data.list.length > 0) {
              var list = that.data.dataArray;
              for (var i = 0; i < response.data.list.length; i++) {
                list.push(response.data.list[i]);
              }
              that.setData({
                dataArray: list,
                searchLoading: false
              });
            }else {
              that.setData({
                searchLoading: false,
                searchLoadingComplete: true
              })
            }
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
    if(that.data.searchLoadingComplete == true) {
      that.setData({
        searchLoadingComplete: false
      })
    }
    console.log("----------")
    console.log(that.data.dataArray)
    console.log("----------")
  },
 /**
   * 获取交易统计
   */
  getOrderStatistical (e) {
    var that = this;
    var sss = {
      "date": that.data.currDate,
      "dateType": 'month'
    }
    var data = {
      jsonPara: JSON.stringify(sss),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('tradeStatistics')
    console.log(wxStorage.getInfoStorageSync('token'))
    wx.request({
      url: 'https://cards.yifubank.com/mch/tradeStatistics',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            if(that.data.dailyMoney == '') {
              that.setData({
                allMoney: response.data.allMoney,
                dailyMoney: response.data.dailyMoney
              })
            }else {
              that.setData({
                allMoney: response.data.allMoney
              })
            }
          }else{
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 选择时间段
   */
  bindDateChange:function (e) {
    var that = this;
    that.setData({
      currDate: e.detail.value
    })
    var that = this;
    that.getOrderStatistical()
    that.onPullDownRefresh()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp);
    //年  
    var Y = date.getFullYear();
    //月  
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
    //日  
    var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    that.setData({
      currDate: Y + '-' + M
    })
    that.getOrderStatistical()
    that.onPullDownRefresh()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  chooseCell: function (e) {
    wx.navigateTo({
      url: '../orderInfo/orderInfo?dict=' + JSON.stringify(e.currentTarget.dataset.bean),
    })
  }
})