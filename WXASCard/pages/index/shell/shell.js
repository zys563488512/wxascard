Page({

  /**
   * 页面的初始数据
   */
  data: {
    isFold: true,
    cardInfo: {
      content: "了解易富通上海易富通金融支付终端— —隶属于上海懿富实业有限公司 主要为客户办理，网贷系统开发，预付费卡，扫码支付，网关支付等业务。。",
      email: "yubo@yifubank.com",
      firstName: "于波",
      hostNumber: "02161270299",
      imageList:
        ["../static/images/about2.jpg", "../static/images/about3.jpg", "../static/images/about4.jpg"],
      logoImage: "../static/images/about2.jpg",
      mobilePhoneNumber: "13636668465",
      organization: "易富通金融",
      title: "产品经理",
      username: "于波",
      weChatNumber: "13636668465",
      workAddressStreet: "上海市闵行区雅致路215号1403"
    }
  },
  makePhoneCall(event) {
    wx.makePhoneCall({
      phoneNumber: event.currentTarget.dataset.num,
      success() {
        console.log('成功拨打电话')
      }
    })
  },
  copy(event) {
    wx.setClipboardData({
      data: event.currentTarget.dataset.dt,
      success() {
        wx.showToast({
          title: '复制成功',
          icon: 'success',
          duration: 1000
        })
      }
    })
  },
  submit(e) {
    const formData = e.detail.value
    wx.addPhoneContact({
      ...formData,
      success() {
        wx.showToast({
          title: '联系人创建成功'
        })
      },
      fail() {
        wx.showToast({
          title: '联系人创建失败'
        })
      }
    })
  },
  handleTapShareButton() {
    if (!((typeof wx.canIUse === 'function') && wx.canIUse('button.open-type.share'))) {
      wx.showModal({
        title: '当前版本不支持转发按钮',
        content: '请升级至最新版本微信客户端',
        showCancel: false
      })
    }
  },
  showAll: function (e) {
    this.setData({
      isFold: !this.data.isFold,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})