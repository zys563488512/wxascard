// pages/index/aouth/aouth.js
var commonCityData = require('../../../utils/addressData.js')
var wxRequest = require('../../../utils/request.js')
var wxStorage = require('../../../utils/storage.js')
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},
    // regionId: ['', '', ''],
    region: ['上海市', '上海市', '徐汇区'],
    bankArray: ["工商银行", "农业银行", "招商银行", "建设银行", "交通银行", "中信银行", "光大银行", "北京银行", "平安银行", "中国银行", "兴业银行", "民生银行", "华夏银行", "广发银行", "浦发银行", "邮储银行", "广州银行"],
    // addressId: [, , ,],
    selDistrictIndex: 0,
    address: '',
    // chooseBankName: '平安银行',
    fixed: null,
    fixAddressid: null,
    country: 0,
    switchStatus: 0,
    status: false,
    // mobile: '',
    userName: '',
    idCard: '',
    email: '',
    provinceCode: '310000',
    cityCode: '310100',
    districtCode: '310104',
    bankNumber: '',
    yanCode: '',
    account_bank: '请选择开户银行',//开户行
    date: '请选择日期',
    fun_id: 2,
    time: '获取验证码', //倒计时 
    currentTime: 61
  },
  //事件处理函数
  onLoad: function (options) {
    var that = this;
    that.setData({
      // mobile: options.mobile,
      // email:options.mobile + '@163.com',
      address:'置业大撒'
    })
  },
  // 获取验证码
  // getCode: function (options) {
  //   var that = this;
  //   var currentTime = that.data.currentTime
  //   var interval = setInterval(function () {
  //     currentTime--;
  //     that.setData({
  //       time: currentTime + '秒'
  //     })
  //     if (currentTime <= 0) {
  //       clearTimeout(interval)
  //       clearInterval(interval)
  //       that.setData({
  //         time: '重新发送',
  //         currentTime: 61,
  //         disabled: false
  //       })
  //     }
  //   }, 1000)
  // },
  // getVerificationCode: function (mobile) {
  //   var that = this;
  //   var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
  //   if (!myreg.test(that.data.mobile)) {
  //     wx.showModal({
  //       title: '提示',
  //       content: '请填写手机号码',
  //       showCancel: false
  //     })
  //     return
  //   }
  //   var sss = {
  //     "mobile": that.data.mobile
  //   }
  //   console.log('verificationCode')
  //   console.log(wxStorage.getInfoStorageSync('token'))
  //   wx.request({
  //     url: 'https://cards.yifubank.com/mch/verificationCode',
  //     data: {
  //       jsonPara: JSON.stringify(sss),
  //       token: wxStorage.getInfoStorageSync('token')
  //     },
  //     method: 'POST',
  //     success(response) {
  //       wx.hideLoading()
  //       console.log(response.data)
  //       console.log('response.code' + response.code)
  //       console.log('response.statusCode' + response.statusCode)
  //       if (response.statusCode == 200) {
  //         if (response.data.code == "0000") {
  //           wx.showToast({
  //             title: '验证码已下发',
  //           })
  //         }
  //       } else {
  //         wx.showModal({
  //           title: '服务器错误',
  //           content: response.data.msg + '',
  //           showCancel: false
  //         })
  //       }
  //     }
  //   })
  //   this.getCode();
  //   var that = this;
  //   that.setData({
  //     disabled: true
  //   })
  // },
  getUserInfo: function (e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  // 保存地址
  bindSave: function (e) {
    var that = this;
    that.data.userName = e.detail.value.userName;
    // that.data.address = e.detail.value.address;
    that.data.idCard = e.detail.value.idCard;
    // that.data.email = e.detail.value.email;
    that.data.bankNumber = e.detail.value.bankNumber;
    // that.data.yanCode = e.detail.value.yanCode;
    if (that.data.userName == "") {
      wx.showModal({
        title: '提示',
        content: '请填写联系人姓名',
        showCancel: false
      })
      return
    }
    if (that.data.idCard == "") {
      wx.showModal({
        title: '提示',
        content: '请填写身份证号码',
        showCancel: false
      })
      return
    }
    // if (that.data.email == "") {
    //   wx.showModal({
    //     title: '提示',
    //     content: '请填写邮箱',
    //     showCancel: false
    //   })
    //   return
    // }
    // if (that.data.address == "") {
    //   wx.showModal({
    //     title: '提示',
    //     content: '请填写详细地址',
    //     showCancel: false
    //   })
    //   return
    // }
    if (that.data.account_bank == "请选择开户银行") {
      wx.showModal({
        title: '提示',
        content: '请选择开户银行',
        showCancel: false
      })
      return
    }
    if (that.data.bankNumber == "") {
      wx.showModal({
        title: '提示',
        content: '请填写银行卡号',
        showCancel: false
      })
      return
    }
    // if (that.data.yanCode == "") {
    //   wx.showModal({
    //     title: '提示',
    //     content: '请填写验证码',
    //     showCancel: false
    //   })
    //   return
    // }
    var register = {
      "name": that.data.userName,
      "cert_no": that.data.idCard,
      "address": that.data.address,
      // "email": that.data.email,
      "province": that.data.provinceCode,
      "city": that.data.cityCode,
      // "district": that.data.districtCode,
      "account_no": that.data.bankNumber,
      "account_bank": that.data.account_bank
      // ,
      // "mobile": that.data.mobile
    }
    var data1 = {
      jsonPara: JSON.stringify(register),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log('mch/register')
    console.log(wxStorage.getInfoStorageSync('token'))
    wx.request({
      url: 'https://cards.yifubank.com/mch/register',
      data: data1,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        console.log('response.code' + response.code)
        console.log('response.statusCode' + response.statusCode)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            wxStorage.saveStorageSync('activity', response.data.activity)
            wx.navigateTo({
              url: '../certification/certification',
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
    // // 验证验证码
    // var yan = {
    //   "mobile": that.data.mobile,
    //   "code": that.data.yanCode
    // }

    // var data = {
    //   jsonPara: JSON.stringify(yan),
    //   token: wxStorage.getInfoStorageSync('token')
    // }
    // console.log(data)
    // console.log('mch/confirmCode')
    // wx.request({
    //   url: 'https://cards.yifubank.com/mch/confirmCode',
    //   data: data,
    //   method: 'POST',
    //   success(response) {
    //     wx.hideLoading()
    //     console.log(response)
    //     if (response.statusCode == 200) {
    //       if (response.data.code == "0000") {
    //         var register = {
    //           "name": that.data.userName,
    //           "cert_no": that.data.idCard,
    //           "address": that.data.address,
    //           "email": that.data.email,
    //           "province": that.data.provinceCode,
    //           "city": that.data.cityCode,
    //           // "district": that.data.districtCode,
    //           "account_no": that.data.bankNumber,
    //           "account_bank": that.data.account_bank,
    //           "mobile": that.data.mobile
    //         }
    //         var data1 = {
    //           jsonPara: JSON.stringify(register),
    //           token: wxStorage.getInfoStorageSync('token')
    //         }
    //         console.log('mch/register')
    //         console.log(wxStorage.getInfoStorageSync('token'))
    //         wx.request({
    //           url: 'https://cards.yifubank.com/mch/register',
    //           data: data1,
    //           method: 'POST',
    //           success(response) {
    //             wx.hideLoading()
    //             console.log(response.data)
    //             console.log('response.code' + response.code)
    //             console.log('response.statusCode' + response.statusCode)
    //             if (response.statusCode == 200) {
    //               if (response.data.code == "0000") {
    //                 wx.navigateTo({
    //                   url: '../index/certification/certification',
    //                 })
    //               } else {
    //                 wx.showModal({
    //                   title: '',
    //                   content: response.data.msg + '',
    //                   showCancel: false
    //                 })
    //               }
    //             } else {
    //               wx.showModal({
    //                 title: '服务器错误',
    //                 content: response.data.msg + '',
    //                 showCancel: false
    //               })
    //             }
    //           }
    //         })
    //       } else {
    //         wx.showModal({
    //           title: '',
    //           content: response.data.msg + '',
    //           showCancel: false
    //         })
    //       }

    //     } else {
    //       wx.showModal({
    //         title: '服务器错误',
    //         content: response.data.msg + '',
    //         showCancel: false
    //       })
    //     }
    //   }
    // })
  },
  getMchInfo4Register: function (e) {
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log("res" + res.code)
        wxRequest.req('getAuthRuleInfo', {
          code: res.code,
        }, 'POST', function (res) {
          console.log(res);
          if (res.data.errcode != 0) {
            wx.showModal({
              title: '修改失败',
              content: res.data.msg,
              showCancel: false
            })
            return;
          }
          wx.navigateBack({})
        })
      }
    })
  },
  bindRegionChange: function (e) {
    console.log('e.detail.value' + e.detail.value)
    this.setData({
      region: e.detail.value,
      provinceCode: e.detail.code[0],
      cityCode: e.detail.code[0],
      districtCode: e.detail.code[0],
      addressId: commonCityData.getAddressIdArray(e.detail.value)
    })
    console.log(this.data.addressId)
    console.log('code' + e.detail.code);

  },
  // 7352.25+6200 = 13552.25
  // 13552.25 - 11824.50	= 1728
  // 1575 + 287.50 = 1862
  bindPickerChange: function (e) {
    var that = this;
    console.log("bankName ---" + that.data.bankArray[e.detail.value])
    that.setData({
      account_bank: that.data.bankArray[e.detail.value]
    })
  },
  // getMobile: function (e) {
  //   this.setData({
  //     mobile: e.detail.value
  //   })
  // }
})