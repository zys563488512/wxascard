// pages/index/updateCard/updateCard.js
var wxStorage = require('../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    account_no: '',
    account_bank: '工商银行',
    mobile: '',
    bankArray: ["工商银行", "农业银行", "招商银行", "建设银行", "交通银行", "中信银行", "光大银行", "北京银行", "平安银行", "中国银行", "兴业银行", "民生银行", "华夏银行", "广发银行", "浦发银行", "邮储银行", "广州银行"],
    mobile: ''
  },
    /**
       * 修改借记卡
       */
  updateCard: function (e) {
    var that = this;
    wx.showLoading({
      title: '',
    })
    that.data.account_no = e.detail.value.account_no
    that.data.account_bank = e.detail.value.account_bank
    that.data.mobile = e.detail.value.mobile

    if (that.data.account_no == "") {
      wx.showToast({
        title: '请输入银行卡号',
      })
      return;
    }
    if (that.data.mobile == "") {
      wx.showToast({
        title: '请输入银行预留手机号',
      })
      return;
    }
    var sss = {
      account_no: that.data.account_no,
      account_bank: that.data.account_bank,
      mobile: that.data.mobile
    }
    console.log('updataMchSettlement')
    wx.request({
      url: 'https://cards.yifubank.com/mch/updataMchSettlement',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        wx.hideLoading()
        if (response.statusCode == 200) {
          wx.showModal({
            title: '',
            content: response.data.msg + '',
            showCancel: false
          })
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  bindPickerChange: function (e) {
    var that = this;
    console.log("bankName ---" + that.data.bankArray[e.detail.value])
    that.setData({
      account_bank: that.data.bankArray[e.detail.value]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})