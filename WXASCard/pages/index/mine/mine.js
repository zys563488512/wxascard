// pages/index/mine/mine.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      userInfo: app.globalData.userInfo
    })
    console.log(that.data.userInfo)
  },
  /**
   * 跳转到个人信息页
   */ 
  getUserInfo: function () {
    wx.navigateTo({
      url: '../myInfo/myInfo',
    })
  },
  /**
   * 我的红包
   */
  redPacket: function () {
    wx.navigateTo({
      url: 'redPacket/redPacket',
    })
  },
  /**
   * 修改借记卡
   */ 
  updateCard: function () {
    wx.navigateTo({
      url: '../updateCard/updateCard',
    })
  },

  /**
   * 卡包
   */
  cardList: function () {
    wx.navigateTo({
      url: '../bankList/bankList?isFrom=mine',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})