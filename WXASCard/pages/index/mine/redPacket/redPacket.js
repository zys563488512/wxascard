// pages/index/mine/redPacket/redPacket.js
var wxRequest = require('../../../../utils/request.js')
var wxStorage = require('../../../../utils/storage.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataArray: [],
    disconut:"0.00"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.getRedPacketList();
  },
  /**
   * 获取红包消费记录
   */ 
  getRedPacketList: function() {
    var that = this;
    console.log("mch/getGiftDetail")
    var sss = {
      type: '1'
    }
    wx.request({
      url: 'https://cards.yifubank.com/mch/getGiftDetail',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log('response.statusCode' + response.statusCode)
        console.log(response)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              dataArray: response.data.data,
              disconut: response.data.disconut
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})