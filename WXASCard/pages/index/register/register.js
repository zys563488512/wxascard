// pages/index/register/register.js
var commonCityData = require('../../../utils/addressData.js')
var wxRequest = require('../../../utils/request.js')
var wxStorage = require('../../../utils/storage.js')
//获取应用实例
const app = getApp()

Page({
  data: {
    userInfo: {},

    selDistrictIndex: 0,

    fixed: null,
    fixAddressid: null,
    country: 0,
    switchStatus: 0,
    status: false,
    mobile: '',
    bankNumber: '',
    yanCode: '',
    fun_id: 2,
    time: '获取验证码', //倒计时 
    currentTime: 61
  },
  //事件处理函数
  onLoad: function(e) {
    console.log("onload")
    console.log(wxStorage.getInfoStorageSync('token'))
  },

  // 获取验证码
  getCode: function(options) {
    var that = this;
    var currentTime = that.data.currentTime
    var interval = setInterval(function() {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearTimeout(interval)
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 61,
          disabled: false
        })
      }
    }, 1000)
  },
  getVerificationCode: function(mobile) {
    var that = this;
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    if (!myreg.test(that.data.mobile)) {
      wx.showModal({
        title: '提示',
        content: '请填写手机号码',
        showCancel: false
      })
      return
    }
    var sss = {
      "mobile": that.data.mobile
    }
    console.log('verificationCode')
    console.log(wxStorage.getInfoStorageSync('token'))
    wx.request({
      url: 'https://cards.yifubank.com/mch/verificationCode',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        console.log('response.code' + response.code)
        console.log('response.statusCode' + response.statusCode)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            this.getCode();
            wx.showToast({
              title: '验证码已下发',
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
    var that = this;
    that.setData({
      disabled: true
    })
  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  // 保存地址
  bindSave: function(e) {
    var that = this;
    that.data.yanCode = e.detail.value.yanCode;

    if (that.data.yanCode == "") {
      wx.showModal({
        title: '提示',
        content: '请填写验证码',
        showCancel: false
      })
      return
    }
    // 验证验证码
    var yan = {
      "mobile": that.data.mobile,
      "code": that.data.yanCode
    }

    var data = {
      jsonPara: JSON.stringify(yan),
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('mch/confirmCode')
    wx.request({
      url: 'https://cards.yifubank.com/mch/confirmMchCode',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            wx.navigateTo({
              url: '../aouth/aouth'
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }

        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  getMchInfo4Register: function(e) {
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log("res" + res.code)
        wxRequest.req('getAuthRuleInfo', {
          code: res.code,
        }, 'POST', function(res) {
          console.log(res);
          if (res.data.errcode != 0) {
            wx.showModal({
              title: '修改失败',
              content: res.data.msg,
              showCancel: false
            })
            return;
          }
          wx.navigateBack({})
        })
      }
    })
  },
  bindRegionChange: function(e) {
    console.log('e.detail.value' + e.detail.value)
    this.setData({
      region: e.detail.value,
      provinceCode: e.detail.code[0],
      cityCode: e.detail.code[0],
      districtCode: e.detail.code[0],
      addressId: commonCityData.getAddressIdArray(e.detail.value)
    })
    console.log(this.data.addressId)
    console.log('code' + e.detail.code);

  },
  bindPickerChange: function(e) {
    var that = this;
    console.log("bankName ---" + that.data.bankArray[e.detail.value])
    that.setData({
      account_bank: that.data.bankArray[e.detail.value]
    })
  },
  getMobile: function(e) {
    this.setData({
      mobile: e.detail.value
    })
  }
})