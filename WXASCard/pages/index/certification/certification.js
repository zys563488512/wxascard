// pages/index/certification/certification.js
var wxStorage = require('../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTargetID:'',
    select1: true,
    select2: true,
    select3: true,
    select4: true,
    select5: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 事件处理
   */ 
  nextClick: function() {
    var that = this;
    if (that.data.select1 == false && that.data.select2 == false && that.data.select3 == false && that.data.select4 == false && that.data.select5 == false) {

      wx.request({
        url: 'https://cards.yifubank.com/mch/mchSubmit',
        data: {
          token: wxStorage.getInfoStorageSync('token')
        },
        method: 'POST',
        success(response) {
          wx.hideLoading()
          console.log('response.statusCode' + response.statusCode)
          if (response.statusCode == 200) {
            console.log(response.data)
            if(response.data.code=="0000"){
              wx.navigateTo({
                url: '../addBankCard/addBankCard?isFrom=' + "",
              })
            }else {
              wx.showModal({
                title: '',
                content: response.data.msg + '',
                showCancel: false
              })
            }
          } else {
            wx.showModal({
              title: '服务器错误',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        }
      })
      
    }else {
      wx.showModal({
        title: '',
        content: '请先上传完成认证照片',
        showCancel: false
      })
    }
    
  },
  /**
   * 上传图片
   */
  // uploadMaterialFCert: function () {
    
  // },
  // uploadMaterialNCert: function () {

  // },
  // uploadMaterialFCard: function () {

  // },
  // uploadMaterialNCard: function () {

  // },
  // uploadMaterialSCert: function () {

  // },
  uploadMaterial: function (e) {
    this.setData({
      currentTargetID: e.currentTarget.id
    })
    var that = this;
    wx.showActionSheet({
      // itemList: ['从相册中选择', '拍照'],
      itemList: ['拍照'],
      itemColor: "#CED63A",
      success: function (res) {
        if (!res.cancel) {
          // if (res.tapIndex == 0) {
          //   that.chooseWxImage('album')
          // } else if (res.tapIndex == 1) {
          //   that.chooseWxImage('camera')
          // }
          if (res.tapIndex == 0) {
            that.chooseWxImage('camera')
          } 
        }
      }
    })
  },
  chooseWxImage: function (type) {
    var that = this;
    console.log("type" + type);
    wx.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: [type],
      success: function (res) {
        console.log(res);
        var type = ''
        var index = ''
        if (that.data.currentTargetID == "1") {
          type = 'IDCARD'
          index = '0'
          that.setData({
            select1 : false
          })
        } else if (that.data.currentTargetID == "2") {
          type = 'IDCARD'
          index = '1'
          that.setData({
            select2 : false
          })
        } else if (that.data.currentTargetID == "3") {
          type = 'SETTLE_BANK_ACCOUNT'
          index = '0'
          that.setData({
            select3 : false
          })
        } else if (that.data.currentTargetID == "4") {
          type = 'SETTLE_BANK_ACCOUNT'
          index = '1'
          that.setData({
            select4: false
          })
        } else if (that.data.currentTargetID == "5") {
          type = 'HEAD_IDCARD'
          index = '0'
          that.setData({
            select5 : false
          })
        }
        var cert = {
          "type": type,
          "index": index,
          "pic": res.tempFilePaths[0]
        }
        wx.request({
          url: 'https://cards.yifubank.com/mch/uploadMaterial',
          data: {
            jsonPara: JSON.stringify(cert),
            token: wxStorage.getInfoStorageSync('token')
          },
          method: 'POST',
          success(response) {
            wx.hideLoading()
            console.log('response.statusCode' + response.statusCode)
            if (response.statusCode == 200) {
              console.log(response.data)
            } else {
              wx.showModal({
                title: '服务器错误',
                content: response.data.msg + '',
                showCancel: false
              })
            }
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})