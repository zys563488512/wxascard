//index.js
var wxStorage = require('../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
  },
  // 进入
  bindGetUserInfo: function (e) {
    var that = this;
    // if (wxStorage.getInfoStorageSync('token') != null) {
    wx.authorize({
      // scope: 'scope.userInfo',
      scope: '',
      success: function (res) {
        wx.switchTab({
          url: '../index/deal/deal',
        })
      },
      fail() {

        console.log("拒绝收取那")

      },
      complete: res => {
        console.log("完成")
        console.log(res)
        wx.switchTab({
          url: '../index/deal/deal'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    // var sss = {
    //   version:'1.6'
    // }
    // console.log('mch/getXiaoChenXu')
    // wx.request({
    //   url: 'https://cards.yifubank.com/getXiaoChenXu',
    //   data: sss,
    //   method: 'POST',
    //   success(response) {
    //     wx.hideLoading()
    //     console.log(response)
    //     if (response.statusCode == 200) {
    //       if (response.data == "0") {
    //         wx.navigateTo({
    //           url: 'shell/shell',
    //         })
    //         return
    //       } 
    //     } else {
    //       wx.showModal({
    //         title: '服务器错误',
    //         content: response.data.msg + '',
    //         showCancel: false
    //       })
    //       return
    //     }
    //   }
    // })




    var wxCode = decodeURIComponent(options.q)
    console.log('----------index---------')
    if(wxCode != null) {
      wxCode = wxCode.split("=")[1];
      wxStorage.saveStorageSync('wxCode',wxCode)
    } 
    console.log(wxCode)
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          wx.switchTab({
            url: '../index/deal/deal',
            // url: '../index/recommend/recommend',
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})