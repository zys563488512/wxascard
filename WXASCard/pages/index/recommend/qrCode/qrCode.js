let QR = require('../../../../utils/qrcodeUtils.js')
var wxStorage = require('../../../../utils/storage.js')
Page({

  data: {
    name: "lishi",
    canvasHidden: false,
    imagePath: '',
    placeholder: 'https://cards.yifubank.com/mch/giveRedPacket?materialId='//默认二维码生成文本
  },

  //动态生成二维码
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
    this.getMaterial_id();
    // var initUrl = this.data.placeholder + 
    // this.createQrCode(initUrl, "mycanvas", size.w, size.h);
  },
  //获取推荐人id
  getMaterial_id: function() {
    var that = this;
    wx.showLoading({
      title: '',
    })
    console.log('myMessage')
    wx.request({
      url: 'https://cards.yifubank.com/mch/myMessage',
      data: {
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            var initUrl = that.data.placeholder + response.data.mch.material_id;
            var size = that.setCanvasSize();//动态设置画布大小
            that.createQrCode(initUrl, "mycanvas", size.w, size.h);
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  //适配不同屏幕大小的canvas
  setCanvasSize: function () {
    var size = {};
    try {
      var res = wx.getSystemInfoSync();
      var scale = 750 / 350;
      var width = res.windowWidth / scale;
      var height = width;//canvas画布为正方形
      size.w = width;
      size.h = height;
    } catch (e) {
      // Do something when catch error
      console.log("获取设备信息失败" + e);
    }
    return size;
  },
  createQrCode: function (url, canvasId, cavW, cavH) {
    //调用插件中的draw方法，绘制二维码图片
    QR.api.draw(url, canvasId, cavW, cavH);
    setTimeout(() => { this.canvasToTempImage(); }, 1000);
  },
  //获取临时缓存照片路径，存入data中
  canvasToTempImage: function () {
    var that = this;
    wx.canvasToTempFilePath({
      canvasId: 'mycanvas',
      success: function (res) {
        var tempFilePath = res.tempFilePath;
        console.log(tempFilePath);
        that.setData({
          imagePath: tempFilePath,
          // canvasHidden:true
        });
      },
      fail: function (res) {
        console.log(res);
      }
    });
  },
  //点击图片进行预览，长按保存分享图片
  previewImg: function (e) {
    var img = this.data.imagePath;
    console.log(img);
    wx.previewImage({
      current: img, // 当前显示图片的http链接
      urls: [img] // 需要预览的图片http链接列表
    })
  },

  // 下载二维码

  downloadCode: function (res) {
    var filePath = this.data.imagePath
    console.log('下载中' + filePath)
    wx.saveImageToPhotosAlbum({
      filePath: filePath,
      success: function (res) {
        wx.showToast({
          title: '图片保存成功',
          icon: 'success',
          duration: 2000 //持续的时间
        })
      },
      fail: function (err) {
        console.log(err)
        wx.showToast({
          title: '图片保存失败',
          icon: 'none',
          duration: 2000//持续的时间
        })
      }
    })
  }


})