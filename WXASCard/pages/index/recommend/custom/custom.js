// pages/index/recommend/custom/custom.js
var wxStorage = require('../../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataDict:{},
    dataArray: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.getMyCustom()
  },
  /**
   * 我的客户
   */ 
  getMyCustom: function() {
    var that = this;
    var data = {
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('getMyCustomer')
    wx.request({
      url: 'https://cards.yifubank.com/mch/getMyCustomer',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              dataDict: response.data.data,
              dataArray: response.data.data.list
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})