// pages/index/recommend/fenrun/fenrun.js
var wxStorage = require('../../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    tradFRArray:[],
  dkInfoArray: [],
    txJLArray: [],
    radFRHeight: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  /**
   * 获取商户返佣记录
   */ 
  getMchFenRunList: function() {
    var that = this;
    var data = {
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('getMoneyBack')
    wx.request({
      url: 'https://cards.yifubank.com/mch/getMoneyBack',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              tradFRArray: response.data.data,
              radFRHeight: response.data.data.length * 100
            })
            console.log("-------tradFRArray-------")
            console.log(that.data.tradFRArray)
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 获取商户提现记录
   */
  getRecommendMoneyWithdraw: function () {
    var that = this;
    var data = {
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('getRecommendMoneyWithdraw')
    wx.request({
      url: 'https://cards.yifubank.com/mch/getRecommendMoneyWithdraw',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              txJLArray: response.data.data
              // ,
              // radTXHeight: response.data.data.length * 80
            })
            console.log("-------txJLArray-------")
            console.log(that.data.txJLArray)
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 获取红包消费记录
   */
  getRedPacketList: function () {
    var that = this;
    console.log("mch/getGiftDetail")
    var sss = {
      type:'2'
    }
    wx.request({
      url: 'https://cards.yifubank.com/mch/getGiftDetail',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log('response.statusCode' + response.statusCode)
        console.log(response)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              dkInfoArray: response.data.data
              // ,
              // radFRHeight: that.data.dkInfoArray.length * 89
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getMchFenRunList()
    that.getRecommendMoneyWithdraw()
    that.getRedPacketList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //滑动切换
  swiperTab: function (e) {
    var that = this;
    console.log("------swiperTab----")
    // if(e.detail.current)
    var height = 0.0;
    console.log('------count-------')
    switch(e.detail.current) {
      case 0:
        height = that.data.tradFRArray.length * 100
        console.log(that.data.tradFRArray.length)
        break;
      case 1:
        height = that.data.dkInfoArray.length * 89
        console.log(that.data.dkInfoArray.length)
        break;
      case 2:
        height = that.data.txJLArray.length * 100
        console.log(that.data.txJLArray.length)
        break;
      default:
        break;
    }
    console.log(e)
    console.log('------height-------')
    console.log(height)
    that.setData({
      currentTab: e.detail.current,
      radFRHeight: height
    });
  },
  //点击切换
  clickTab: function (e) {
    var that = this;
    console.log("------clickTab----")
    console.log(e)
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})