// pages/index/recommend/myFR/deposit.js
var wxStorage = require('../../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money:"",
    myInfo:{},
    isShowMoney:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.getMyInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  /**
   * 获取个人信息
   */
  getMyInfo: function () {
    var that = this;
    console.log('myMessage')
    wx.request({
      url: 'https://cards.yifubank.com/mch/myMessage',
      data: {
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response.data)
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              myInfo: response.data.mch
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  /**
   * 提现
   */ 
  bindSave: function () {
    var that = this;
    that.setData({
      ifName:true,
      isShowMoney:true
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  cancel: function() {
    var that = this;
    that.setData({
      ifName: false,
      isShowMoney: false
    })
  },
  confirm: function() {
     
    var that = this;
    that.setData({
      ifName: false,
      isShowMoney: false
    })
    console.log('giftMchWithdraw')
    console.log(that.data.money)
    console.log('giftMchWithdraw')
    var sss = {
      "money": that.data.money
    }
    wx.request({
      url: 'https://cards.yifubank.com/mch/giftMchWithdraw',
      data: {
        jsonPara: JSON.stringify(sss),
        token: wxStorage.getInfoStorageSync('token')
      },
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            wx.showModal({
              title: '支付结果',
              content: '提现成功!',
              showCancel: false,//是否显示取消按钮
              confirmText: "确定",//默认是“确定”
              success: function (res) {
                if (res.cancel) {
                  //点击取消,默认隐藏弹框
                } else {
                  //点击确定
                  wx.navigateBack({
                    
                  })
                }
              },
              fail: function (res) { },//接口调用失败的回调函数
              complete: function (res) { },//接口调用结束的回调函数（调用成功、失败都会执行）
            })          
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },
  depositMoney:function (e) {
    var that = this;
    that.setData({
      money:e.detail.value
    })
  }
})