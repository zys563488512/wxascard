// pages/index/recommend/myFR/myFR.js
var wxStorage = require('../../../../utils/storage.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    all:0,
    myRecommendMoney:"0.00",
    today:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // var that = this;
    // that.getMyFRInfo()
  },

  /**
   * 我的分润信息
   */
  getMyFRInfo: function () {
    var that = this;
    var data = {
      token: wxStorage.getInfoStorageSync('token')
    }
    console.log(data)
    console.log('getMyRecommendMoney')
    wx.request({
      url: 'https://cards.yifubank.com/mch/getMyRecommendMoney',
      data: data,
      method: 'POST',
      success(response) {
        wx.hideLoading()
        console.log(response)
        wx.hideLoading()
        if (response.statusCode == 200) {
          if (response.data.code == "0000") {
            that.setData({
              all: response.data.data.all,
              myRecommendMoney: response.data.data.myRecommendMoney,
              today: response.data.data.today
            })
          } else {
            wx.showModal({
              title: '',
              content: response.data.msg + '',
              showCancel: false
            })
          }
        } else {
          wx.showModal({
            title: '服务器错误',
            content: response.data.msg + '',
            showCancel: false
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // var that = this;
    // that.getMyFRInfo()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    that.getMyFRInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  /**
   * 提现到银行卡
   */ 
  redToBank: function () {
    wx.navigateTo({
      url: 'deposit',
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})