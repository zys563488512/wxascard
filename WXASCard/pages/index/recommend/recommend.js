// pages/index/recommend/recommend.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**
   * 推广政策
   */
  generalizeInfo: function () {
    wx.navigateTo({
      url: 'generalize/generalize',
    })
  },

  /**
   * 分享推广二维码
   */
  shareQrCode: function () {
    wx.navigateTo({
      url: 'qrCode/qrCode',
    })
  },
  /**
   *  我的分润
   */
  myFenRun: function () {
    wx.navigateTo({
      url: 'myFR/myFR',
    })
  },
  /**
   *  我的客户
   */
  myCustom: function () {
    wx.navigateTo({
      url: 'custom/custom',
    })
  },
  /**
   * 分享注册链接
   */
  shareUrl: function () {
    wx.navigateTo({
      url: 'shareUrl/shareUrl',
    })
  },
  handleContact(e) {
    console.log(e.path)
    console.log(e.query)
  },
  /**
   * 分润明细
   */ 
  fenrunInfo: function() {
    wx.navigateTo({
      url: 'fenrun/fenrun',
    })
  }
})